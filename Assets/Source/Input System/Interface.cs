﻿using UnityEngine;

namespace RPG.InputSystem
{
    public interface IInputSource<T>
    {
        T GetData();
    }

    public struct MovementInputData
    {
        public Vector2 direction;
        public Vector2 rotation;
    }

    public struct AttackInputData
    {
        public bool Attack;
        public bool StrongAttack;
    }
}
