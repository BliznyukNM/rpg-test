﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace RPG.InputSystem
{
    public class KeyboardInput : MonoBehaviour, IInputSource<MovementInputData>, IInputSource<AttackInputData>
    {
		[SerializeField] private float rotationSpeed = 5.0f;

		private MovementInputData movementData;
		private AttackInputData attackData;

		private void FixedUpdate()
		{
			movementData.direction = new Vector2(Input.GetAxis("Horizontal"), Input.GetAxis("Vertical"));
			movementData.rotation = new Vector2(Input.GetAxis("Mouse Y") * rotationSpeed / 2.0f, Input.GetAxis("Mouse X") * rotationSpeed);

			attackData.Attack = Input.GetKeyDown(KeyCode.Mouse0);
			attackData.StrongAttack = Input.GetKeyDown(KeyCode.Mouse1);
		}

        MovementInputData IInputSource<MovementInputData>.GetData()
        {
			return movementData;
        }

        AttackInputData IInputSource<AttackInputData>.GetData()
        {
			return attackData;
        }
    }
}
