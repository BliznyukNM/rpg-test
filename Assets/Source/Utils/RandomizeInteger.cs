﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace RPG.Utils
{
    public class RandomizeInteger : StateMachineBehaviour
    {
		[SerializeField] private string intName;
		[SerializeField] private int from;
		[SerializeField] private int to;

		public override void OnStateExit(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
		{
			animator.SetInteger(intName, Random.Range(from, to));
		}
    }
}
