﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace RPG.Utils
{
    public class SetBool : StateMachineBehaviour
    {
        [SerializeField] private string boolName;
        [SerializeField] private bool value;

        override public void OnStateExit(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
        {
            animator.SetBool(boolName, value);
        }
    }
}
