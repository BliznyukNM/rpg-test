﻿using System;
using System.Collections;
using System.Collections.Generic;
using RPG.Character;
using UnityEngine;

namespace RPG.Activation
{
    public class Damage : MonoBehaviour, IActivation, RPG.Character.IHittable
    {
        [SerializeField] private int id;

        public event Action<IActivation> OnActivation;

        private EffectsController effectsController;
        private bool isActivated;

        public int Id => id;
        
        private void Awake()
        {
            effectsController = GetComponentInChildren<EffectsController>();
        }

        void IHittable.Damage()
        {
            if (!isActivated)
                OnActivation?.Invoke(this);
        }

        public void Activate(bool activate, bool triggerEffects)
        {
            isActivated = activate;

            if(triggerEffects)
                effectsController.Emmit(activate);
        }

        public void ClearEffects()
        {
            effectsController.Clear();
        }
    }
}
