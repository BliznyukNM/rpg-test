﻿namespace RPG.Activation
{
    public interface IActivation
    {
        int Id { get; }
        event System.Action<IActivation> OnActivation;
        void Activate(bool activate, bool triggerEffects = true);
        void ClearEffects();
    }
}