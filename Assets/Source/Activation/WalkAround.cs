﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace RPG.Activation
{
    public class WalkAround : MonoBehaviour, IActivation
    {
        [SerializeField] private int id;

        public event Action<IActivation> OnActivation;

        public int Id => id;

        private EffectsController effectsController;
        private bool isActivated;

        private void Awake()
        {
            effectsController = GetComponentInChildren<EffectsController>();
        }

        public void Activate(bool activate, bool triggerEffects = true)
        {
            isActivated = activate;

            if (triggerEffects)
                effectsController.Emmit(activate);
        }

        public void ClearEffects()
        {
            effectsController.Clear();
        }

        private Coroutine walking;

        private void OnTriggerEnter(Collider other)
        {
            if (isActivated)
                return;

            if (walking != null)
                StopCoroutine(walking);
            walking = StartCoroutine(Walking(other.transform));
        }

        private void OnTriggerExit(Collider other)
        {
            if (walking != null)
                StopCoroutine(walking);
        }

        private IEnumerator Walking(Transform target)
        {
            float angle = 360.0f;
            var prevPos = target.position;
            var direction = 1.0f;

            while (angle > 0.0f)
            {
                var radius = transform.position + (target.position - transform.position).normalized;
                var tangent = (radius - transform.position).normalized;
                tangent = new Vector3(-tangent.z, 0.0f, tangent.x);
                var deltaPos = (target.position - prevPos).normalized;

                float dot = Vector3.Dot(tangent, deltaPos);
                if (direction * dot < 0.0f)
                {
                    angle = 360.0f;
                    direction *= -1.0f;
                }

                var a = (target.position - transform.position).normalized;
                var b = (prevPos - transform.position).normalized;

                angle -= Vector3.Angle(a, b);

                prevPos = target.position;

                yield return null;
            }

            OnActivation?.Invoke(this);
        }
    }
}