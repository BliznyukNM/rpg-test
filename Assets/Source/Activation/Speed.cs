﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace RPG.Activation
{
    public class Speed : MonoBehaviour, IActivation
    {
        [SerializeField] private float stayTime = 1.0f;
		[SerializeField] private int id;

		public int Id => id;

        public event Action<IActivation> OnActivation;

        private Coroutine stay;
        private bool isActivated;
        private EffectsController effectsController;

        private void Awake()
        {
            effectsController = GetComponentInChildren<EffectsController>();
        }

        public void Activate(bool activate, bool triggerEffects = true)
        {
            isActivated = activate;

            if (triggerEffects)
                effectsController.Emmit(activate);
        }

        public void ClearEffects()
        {
            effectsController.Clear();
        }

        private void OnTriggerEnter(Collider other)
        {
            if (isActivated)
                return;

            var rb = other.GetComponent<Rigidbody>();

            if (rb == null)
                return;

            if (stay != null)
                StopCoroutine(stay);

            stay = StartCoroutine(Stay(rb));
        }

		private void OnTriggerExit(Collider other)
		{
			if(stay != null)
				StopCoroutine(stay);
		}

        private IEnumerator Stay(Rigidbody target)
        {
            float time = 0.0f;

            while (time < stayTime)
            {
                if (target.velocity.magnitude > 0.5f)
                    time = 0.0f;

                time += Time.deltaTime;
                yield return null;
            }

            OnActivation?.Invoke(this);
        }
    }
}
