﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace RPG
{
    public class IconsOrder : MonoBehaviour
    {
		[SerializeField] private Image[] icons;

		public void Order(List<RPG.Activation.IActivation> activations)
		{
			var result = new Sprite[icons.Length];

			for(int i = 0; i < icons.Length; i++)
			{
				result[i] = icons[activations[i].Id].sprite;
			}

			for(int i = 0; i < icons.Length; i++)
			{
				icons[i].sprite = result[i];
			}
		}
    }
}
