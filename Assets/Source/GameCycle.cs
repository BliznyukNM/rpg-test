﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using RPG.Activation;
using System.Threading.Tasks;

namespace RPG
{
    public class GameCycle : MonoBehaviour
    {
        [SerializeField] private CameraMotor cam;
        [SerializeField] private Spawner spawner;
        [SerializeField] private IconsOrder iconsOrder;

        private List<IActivation> activations;
        private int currentActivationIndex;

        private GameObject character;

        private void Start()
        {
            var objects = spawner.Respawn();

            character = objects.Find(o => o.GetComponent<RPG.Character.Motor>() != null);
            cam.Target = character.transform;
            objects.Remove(character);

            activations = new List<IActivation>();

            for (int i = objects.Count - 1, j = 0; i >= 0; i--)
            {
                var activation = objects[i].GetComponent<IActivation>();

                if (activation == null)
                    continue;

                activations.Add(activation);
                activations[j].OnActivation += ProcessActivation;

                j++;
                objects.RemoveAt(i);
            }

            ShuffleActivations();
        }

        private void ShuffleActivations()
        {
            currentActivationIndex = 0;

            for (int i = 0; i < activations.Count; i++)
            {
                int j = Random.Range(0, i);
                var value = activations[j];
                activations[j] = activations[i];
                activations[i] = value;
            }

            iconsOrder.Order(activations);
        }

        private async void ProcessActivation(IActivation obj)
        {
            int i = activations.IndexOf(obj);

            if (i == currentActivationIndex)
            {
                currentActivationIndex++;
                obj.Activate(true);

                if (currentActivationIndex >= activations.Count)
                {
                    await Task.Delay(1500);
                    activations.ForEach(a =>
                    {
                        a.Activate(false, false);
                        a.ClearEffects();
                    });
                    spawner.Respawn();
                    cam.Target = character.transform;
                }
            }
            else
            {
                activations.ForEach(a =>
                {
                    a.Activate(false, a == obj);
                });
            }
        }
    }
}
