﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using RPG.InputSystem;

namespace RPG
{
    public class CameraMotor : MonoBehaviour
    {
        [SerializeField] private Vector3 offset;
        [SerializeField] private float maxAngle = 40.0f;
        [SerializeField] private float minAngle = -40.0f;

        public Transform Target
        {
            private get
            {
                return target;
            }
            set
            {
				target = value;
				movementInput = target.GetComponent<IInputSource<MovementInputData>>();
                transform.rotation = target.rotation;
            }
        }

        private Transform target;
        private IInputSource<MovementInputData> movementInput;
		private Transform camRigX;
		private Transform cameraRig;

        private void Awake()
        {
			camRigX = transform.GetChild(0);
			cameraRig = camRigX.GetChild(0);

			cameraRig.localPosition = offset;
        }

        private void FixedUpdate()
        {
            if (Target == null)
                return;

			var movementData = movementInput.GetData();

            transform.position = Target.position;
			
            transform.rotation *= Quaternion.Euler(0.0f, movementData.rotation.y, 0.0f);

			camRigX.localRotation *= Quaternion.Euler(-movementData.rotation.x, 0.0f, 0.0f);
            float angle = camRigX.rotation.eulerAngles.x;
            angle = angle > 180.0f ? angle - 360.0f : angle;
            angle = angle < -180.0f ? angle + 360.0f : angle;
            camRigX.localRotation = Quaternion.Euler(Mathf.Clamp(angle, minAngle, maxAngle), 0.0f, 0.0f);
        }
    }
}