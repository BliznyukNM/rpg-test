﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace RPG.Character
{
    public interface IHittable
    {
        void Damage();
    }

    public class Sword : MonoBehaviour
    {
        private void OnTriggerEnter(Collider other)
        {
            var hittable = other.GetComponent<IHittable>();
            if (hittable == null)
                return;
			
			hittable.Damage();
        }
    }
}
