﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using RPG.InputSystem;

namespace RPG.Character
{
    [RequireComponent(typeof(Rigidbody))]
    [RequireComponent(typeof(Animator))]
    public class Motor : MonoBehaviour
    {
        private IInputSource<MovementInputData> movementInput;
        private IInputSource<AttackInputData> attackInput;

        private Animator ac;

        private void Awake()
        {
            ac = GetComponent<Animator>();

            movementInput = GetComponent<IInputSource<MovementInputData>>();
            attackInput = GetComponent<IInputSource<AttackInputData>>();
        }

        private void FixedUpdate()
        {
            var movementData = movementInput.GetData();
            var attackData = attackInput.GetData();

            // Filter data if we need it.

            ac.SetFloat("Movement Vertical", movementData.direction.y);
            ac.SetFloat("Movement Horizontal", movementData.direction.x);

            transform.rotation *= Quaternion.Euler(0.0f, movementData.rotation.y, 0.0f);

            if (attackData.Attack)
            {
                ac.SetBool("Attack", true);
            }

            if (attackData.StrongAttack && ac.GetCurrentAnimatorClipInfo(0)[0].clip.name.StartsWith("Attack"))
            {
                ac.SetBool("Strong Attack", true);
            }
        }
    }
}
