﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace RPG.Character
{
    public class Weapon : MonoBehaviour
    {
		[SerializeField] private Collider weaponCollider;

		public void EnableCollision()
		{
			weaponCollider.enabled = true;
		}

		public void DisableCollision()
		{
			weaponCollider.enabled = false;
		}
    }
}
