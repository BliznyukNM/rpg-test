﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace RPG
{
    public class Spawner : MonoBehaviour
    {
        [SerializeField] private Vector2 grid;
        [SerializeField] private Vector2 gridStep;
        [SerializeField] private Transform[] objectsToSpawn;

        private Transform[] spawnedObjects;

        private void Awake()
        {
            spawnedObjects = new Transform[objectsToSpawn.Length];
        }

        public List<GameObject> Respawn()
        {
            var positions = GetPositions();
            var objects = new List<GameObject>();

            for (int i = 0; i < objectsToSpawn.Length; i++)
            {
                int randIndex = Random.Range(0, positions.Count);

                Transform obj;
                if (spawnedObjects[i] == null)
                {
                    obj = Instantiate(objectsToSpawn[i]);
                    spawnedObjects[i] = obj;
                }
                else
                {
                    obj = spawnedObjects[i];
                }

                obj.position = positions[randIndex];
                obj.rotation = Quaternion.identity;

                objects.Add(obj.gameObject);

                positions.RemoveAt(randIndex);
            }

            return objects;
        }

        private List<Vector3> GetPositions()
        {
            var positions = new List<Vector3>();

            for (float x = -grid.x; x <= grid.x; x += gridStep.x)
            {
                for (float y = -grid.y; y <= grid.y; y += gridStep.y)
                {
                    positions.Add(new Vector3(x, 0.0f, y));
                }
            }

            return positions;
        }

#if UNITY_EDITOR

        private Mesh m;

        private void OnDrawGizmosSelected()
        {
            if (m == null)
            {
                m = new Mesh();

                m.vertices = new Vector3[4];

                m.triangles = new int[]
                {
                    0, 1, 3,
                    1, 2, 3
                };

                m.RecalculateNormals();
            }

            m.vertices = new Vector3[]
            {
                new Vector3(grid.x, 0.1f, grid.y),
                new Vector3(grid.x, 0.1f, -grid.y),
                new Vector3(-grid.x, 0.1f, -grid.y),
                new Vector3(-grid.x, 0.1f, grid.y)
            };

            Gizmos.color = Color.yellow;
            Gizmos.DrawWireMesh(m, transform.position);

            for (float x = -grid.x; x <= grid.x; x += gridStep.x)
            {
                for (float y = -grid.y; y <= grid.y; y += gridStep.y)
                {
                    Gizmos.DrawSphere(transform.position + new Vector3(x, 0.1f, y), 0.2f);
                }
            }
        }

#endif
    }
}
