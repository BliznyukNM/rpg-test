﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace RPG
{
    public class EffectsController : MonoBehaviour
    {
		[System.Serializable]
		private struct Effect
		{
			public bool type;
			public GameObject particles;
		}

		[SerializeField] private Effect[] effects;

		public void Emmit(bool activated)
		{
			for(int i = 0; i < effects.Length; i++)
			{
				effects[i].particles.SetActive(effects[i].type == activated);
			}
		}

		public void Clear()
		{
			for(int i = 0; i < effects.Length; i++)
			{
				effects[i].particles.SetActive(false);
			}
		}
    }
}
